// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
var jwt = require('jsonwebtoken');
var path = require('path');

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({extended: false});

// Log requests
app.all("*", urlencodedParser, function (req, res, next) {
    console.log(req.method + " " + req.url);
    console.dir(req.headers);
    console.log(req.body);
    console.log();
    next();
});

// Serve static files
app.use('/static', express.static('public'));
app.use(cookieParser());

app.get("/users", function (req, res, next) {
    db.all('SELECT rowid, ident, password FROM users;', function (err, data) {
        res.json(data);
        if (err) console.error(err);
    });
});

// Connexion de l'utilisateur
app.post("/login", function (req, res, next) {
    db.get('SELECT rowid, ident, password FROM users WHERE ident = ? AND password = ? ', [req.body.identifiant, req.body.password],
        function (err, data) {
            if (data != null) {

                var token = jwt.sign({ident: data.ident}, 'RESTFULAPIs');

                var identifiant = data.ident;

                db.get('SELECT * FROM sessions WHERE ident = ?', [identifiant], function (err, dataSession) {
                    if (dataSession == null)
                        db.run("INSERT INTO sessions (ident, token) VALUES (?, ?)", [identifiant, token], function (err, result) {
                        });
                    else
                        db.run("UPDATE sessions SET token = ? WHERE ident = ?", [token, identifiant], function (err, result) {
                        });
                    if (err) console.error(err);
                });
                res.cookie("KeySession", token);
                console.log("Création du cookie");

                // res.json({status: 'true', token: token});

                res.sendFile(path.join(__dirname, 'public', 'logout.html'));
            }
            else res.json({status: 'false'});

            if (err) console.error(err);
        });
});

// Deconnexion de l'utilisateur
app.post("/logout", function (req, res, next) {

    var cookieSession = req.cookies.KeySession;

    db.get('DELETE FROM sessions WHERE token = ? ', [cookieSession],
        function (err, data) {

            if (err) console.error(err);

            res.clearCookie("KeySession");

            res.sendFile(path.join(__dirname, 'public', 'index.html'));
        });
});

// Redirection si un token correspond au cookie dans le navigateur
app.get('/', function (req, res, next) {

    var cookieSession = req.cookies.KeySession;

    db.get('SELECT * FROM sessions WHERE token = ?', [cookieSession], function (err, data) {

        if (data != null) {
            res.sendFile(path.join(__dirname, 'public', 'logout.html'));
        }
        else{
                res.sendFile(path.join(__dirname, 'public', 'index.html'));
            }

        if (err) console.error(err);
    });
});

// Startup server
app.listen(port, function () {
    console.log('Le serveur est accessible sur http://localhost:' + port + "/");
    console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
